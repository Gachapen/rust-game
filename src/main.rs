extern crate vulkano;
extern crate winit;
extern crate vulkano_win;

use vulkano::instance as vki;
use vulkano::device as vkd;
use vulkano::swapchain as vks;

use vulkano_win::VkSurfaceBuild;

fn main() {
    println!("Hello, World!");

    let instance = {
        let extensions = vulkano_win::required_extensions();
        vki::Instance::new(None, &extensions, None).expect("Failed to create Vulkan instance")
    };

    // Use first for now, but should look for suitable device and possibly ask user.
    let phys_device = vki::PhysicalDevice::enumerate(&instance).next().expect("No device available");
    println!("Using Vulkan device: {} (type: {:?})", phys_device.name(), phys_device.ty());

    let window = winit::WindowBuilder::new().build_vk_surface(&instance).unwrap();

    let queue_family = phys_device.queue_families().find(|candidate| {
        candidate.supports_graphics() && window.surface().is_supported(candidate).unwrap_or(false)
    }).expect("Could not find a suitable queue family");

    let (device, mut queue_list) = {
        let device_ext = vkd::DeviceExtensions {
            khr_swapchain: true,
            .. vkd::DeviceExtensions::none()
        };

        vkd::Device::new(
            &phys_device,
            phys_device.supported_features(),
            &device_ext,
            [(queue_family, 0.5)].iter().cloned()
        ).expect("Failed to create device")
    };

    let queue = queue_list.next().unwrap();

    let (swapchain, images) = {
        let capabilities = window.surface().get_capabilities(&phys_device).expect("Failed to get surface capabilities");
        let dimensions = capabilities.current_extent.unwrap_or([1280, 720]);
        let present = capabilities.present_modes.iter().next().unwrap();
        let alpha = capabilities.supported_composite_alpha.iter().next().unwrap();
        let format = capabilities.supported_formats[0].0;
        let num_images = 2;
        let num_layers = 1;
        let clipped = true;
        let prev_swapchain = None;

        vks::Swapchain::new(
            &device,
            &window.surface(),
            num_images,
            format,
            dimensions,
            num_layers,
            &capabilities.supported_usage_flags,
            &queue,
            vks::SurfaceTransform::Identity,
            alpha,
            present,
            clipped,
            prev_swapchain
        ).expect("Failed to create swapchain")
    };
}
